extern crate rand;

use raspi_life::rules::Rules;

pub fn main() {
    let width: u32 = 8;
    let height: u32 = 8;
    
    let rules = Rules::create(3, 2, 3);

    raspi_life::run(width, height, rules);
}

