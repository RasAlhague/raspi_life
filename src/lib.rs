pub mod board;
pub mod rules;

use rand::Rng;
use crate::board::Board;
use crate::rules::Rules;

pub fn run(width: u32, height: u32, rules: Rules) {
    let mut board = Board::setup(width, height);

    birth_random_cells(16, &mut board);

    for _i in 0..20 {
        board.print();
        board.next_generation(&rules);
    }
}

pub fn birth_random_cells(num_of_cells: u32, board: &mut Board) {
    let mut rng = rand::thread_rng(); 

    for _x in 0..num_of_cells {
        board.birth_cell(rng.gen_range(0, board.width()), rng.gen_range(0, board.height()));
    }
}