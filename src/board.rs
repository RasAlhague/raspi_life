use std::convert::TryInto;
use crate::rules::Rules;

pub struct Board {
    cells: Vec<Vec<u32>>,
    generation: u32,
    height: u32,
    width: u32,
}

impl Board {
    pub fn setup(width: u32, height: u32) -> Board {
        let mut cells: Vec<Vec<u32>> = Vec::new();

        for _y in 0..height {
            let mut row: Vec<u32> = Vec::new();
            
            for _x in 0.. width {
                row.push(0);
            }
            
            cells.push(row);
        }

        Board {
            cells,
            generation: 0,
            width,
            height,
        }
    }
    
    pub fn next_generation(&mut self, rules: &Rules) {
        let mut new_cells = self.cells.clone();
        
        for y in 0..self.height {
            for x in 0..self.width {
                let row = y as usize;
                let col = x as usize;
                let count_living_cells: u32 = self.get_living_cells(y as i32, x as i32);
                
                if let Some(r) = rules.match_rules(self.cells[row][col], count_living_cells) {
                    new_cells[row][col] = r;
                }
            }
        }
        self.cells = new_cells;
        
        self.generation += 1;
    }

    pub fn print(&self) {
        println!("Generation: {};", self.generation);

        for y in 0..self.cells.len() {
            for x in 0..self.cells[y].len() {
                print!("{}", self.cells[y as usize][x as usize]);
            }
            println!("");
        }
    }

    pub fn generation(&self) -> u32 {
        self.generation
    }

    pub fn kill_cell(&mut self, col: u32, row: u32) {
        if col > 0 && col < self.width && row > 0 && row < self.height {
            self.cells[row as usize][col as usize] = 0;
        } 
    }

    pub fn birth_cell(&mut self, col: u32, row: u32) {
        if col > 0 && col < self.width && row > 0 && row < self.height {
            self.cells[row as usize][col as usize] = 1;
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    fn get_living_cells(&self, row: i32, col: i32) -> u32 {
        let mut living_cells: u32 = 0;

        living_cells = self.increase_cell_counter(living_cells, row - 1, col - 1);
        living_cells = self.increase_cell_counter(living_cells, row - 1, col);
        living_cells = self.increase_cell_counter(living_cells, row - 1, col + 1);
        living_cells = self.increase_cell_counter(living_cells, row, col - 1);
        living_cells = self.increase_cell_counter(living_cells, row, col + 1);
        living_cells = self.increase_cell_counter(living_cells, row + 1, col - 1);
        living_cells = self.increase_cell_counter(living_cells, row + 1, col);
        living_cells = self.increase_cell_counter(living_cells, row + 1, col + 1); 

        return living_cells;
    }

    fn get_cell_state(&self, row: i32, col: i32) -> Option<u32> {
        if row < 0 || col < 0 || row == self.height.try_into().unwrap() || col == self.width.try_into().unwrap() {
            return None;
        }
        else {
            return Some(self.cells[row as usize][col as usize]);
        }
    }

    fn increase_cell_counter(&self, living_cells: u32, row: i32, col: i32) -> u32 {
        if let Some(s) = self.get_cell_state(row, col) {
            return living_cells + s;
        }
        else {
            return living_cells;
        }
    }
}