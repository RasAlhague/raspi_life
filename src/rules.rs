pub struct Rules {
    rebirth_at: u32,
    lonely_at_less: u32,
    too_many_at_more: u32,
}

impl Rules {
    pub fn create(rebirth_at: u32, lonely_at_less: u32, too_many_at_more: u32) -> Rules {
        Rules {
            rebirth_at,
            lonely_at_less,
            too_many_at_more,
        }
    }

    pub fn match_rules(&self, cell_state: u32, living_neighbours: u32) -> Option<u32> {
        if cell_state == 1 {
            if living_neighbours < self.lonely_at_less {
                return Some(0);
            }
            else if living_neighbours > self.too_many_at_more {
                return Some(0);
            }
            else {
                return None;
            }
        }
        else {
            if living_neighbours == self.rebirth_at {
                return Some(1);
            }
            else {
                return None;
            }
        }
    }
}